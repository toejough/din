"""
Test Din.

Desired Behaviors:
* equality mixin
  * an object is equal to itself
  * two objects of the same type with equal attributes are equal
  * differing types are not implemented
  * which attributes contribute to equality is settable
* repr mixin
  * module & class name in repr
  * attributes listed by name & repr value
  * which attributes are in repr is settable
  * if the name and attrs can fit on one line, they do
  * if a stringified value is too long, it is broken up.
  * list values have specific single and multi-line formating
  * tuple values have specific single and multi-line formating
  * set values have specific single and multi-line formating
  * dict values have specific single and multi-line formating
"""


# [ Imports:Python ]
import typing

# [ Imports:Project ]
import din


# [ Helpers ]
class _EqualityTestObject(din.EqualityMixin):
    def __init__(self, thing_a: typing.Any, thing_b: typing.Any, **kwargs: typing.Any) -> None:
        super().__init__(**kwargs)
        self.thing_a = thing_a
        self.thing_b = thing_b
        # Vulture
        # pylint: disable=pointless-statement
        self.thing_a
        self.thing_b
        # pylint: enable=pointless-statement


class _EqualityTestObjectSubclass(_EqualityTestObject):
    def __init__(
            self,
            thing_a: typing.Any,
            thing_b: typing.Any,
            thing_c: typing.Any,
            **kwargs: typing.Any,
    ) -> None:
        super().__init__(thing_a, thing_b, **kwargs)
        self.thing_c = thing_c
        # Vulture
        self.thing_c  # pylint: disable=pointless-statement


class _EqualityFilterTestObject(din.EqualityMixin):
    def __init__(self, thing_a: typing.Any, thing_b: typing.Any, **kwargs: typing.Any) -> None:
        super().__init__(**kwargs)
        self.thing_a = thing_a
        self.thing_b = thing_b
        self._equality_attributes = ('thing_a',)
        # Vulture
        # this object is explicitly here to test the equality attributes
        self._equality_attributes  # pylint: disable=pointless-statement


class _ReprTestObject(din.ReprMixin):
    def __init__(self, **kwargs: typing.Any) -> None:
        super().__init__()
        for key, value in kwargs.items():
            setattr(self, key, value)

    def __getattr__(self, name: str) -> typing.Any:
        if name not in self.__dict__:
            raise AttributeError(f"{name} not found")
        return self.__dict__[name]

    def set_indent(self, indentation: str) -> None:
        self._repr_indent = indentation
        # Vulture
        # this object is explicitly here to test setting this
        self._repr_indent  # pylint: disable=pointless-statement

    def customize_attributes(self, *attributes: str) -> None:
        self._repr_attributes = attributes
        # Vulture
        # this object is explicitly here to test setting this
        self._repr_attributes  # pylint: disable=pointless-statement


class _ReprRecursiveTestObject(din.ReprMixin):
    def __init__(self, **kwargs: typing.Any) -> None:
        super().__init__()
        for key, value in kwargs.items():
            setattr(self, key, value)
        self.recursive = self
        # for Vulture
        # pylint: disable=pointless-statement
        self.recursive
        # pylint: enable=pointless-statement

    def __getattr__(self, name: str) -> typing.Any:
        if name not in self.__dict__:
            raise AttributeError(f"{name} not found")
        return self.__dict__[name]

    def set_indent(self, indentation: str) -> None:
        self._repr_indent = indentation

    def customize_attributes(self, *attributes: str) -> None:
        self._repr_attributes = attributes


class _ReprFilterTestObject(din.ReprMixin):
    def __init__(self, thing_a: typing.Any, thing_b: typing.Any, **kwargs: typing.Any) -> None:
        super().__init__(**kwargs)
        self.thing_a = thing_a
        self.thing_b = thing_b
        self._repr_attributes = ('thing_b',)


class _FullNonRepr:
    def __init__(self) -> None:
        self.value = "a value"


# [ Tests ]
def test_self_equal() -> None:
    # Given
    # an object
    obj = _EqualityTestObject(1, 2)

    # When
    # the object is compared to itself
    equal = obj == obj  # pylint: disable=comparison-with-itself

    # Then
    assert equal


def test_equal_objects_equal() -> None:
    # Given
    # an object
    obj1 = _EqualityTestObject(1, 2)
    obj2 = _EqualityTestObject(1, 2)

    # When
    # the object is compared to itself
    equal = obj1 == obj2

    # Then
    assert equal


def test_unequal_objects_unequal() -> None:
    # Given
    # an object
    obj1 = _EqualityTestObject(1, 2)
    obj2 = _EqualityTestObject(2, 1)

    # When
    # the object is compared to itself
    equal = obj1 == obj2

    # Then
    assert not equal


def test_subclass_equal() -> None:
    # Given
    # an object
    obj = _EqualityTestObject(1, 2)
    sub_obj = _EqualityTestObjectSubclass(1, 2, 3)

    # When
    # the object is compared to a subclass item
    equal = obj == sub_obj

    # Then
    assert equal


def test_subclass_unequal() -> None:
    # Given
    # an object
    obj = _EqualityTestObject(1, 2)
    sub_obj = _EqualityTestObjectSubclass(4, 2, 3)

    # When
    # the object is compared to a subclass item
    equal = obj == sub_obj

    # Then
    assert not equal


def test_other_type_unequal() -> None:
    # Given
    # an object
    obj = _EqualityTestObject(1, 2)
    other = (1, 2)

    # When
    # the object is compared to a subclass item
    equal = obj == other

    # Then
    assert not equal


def test_equal_with_chosen_attributes_equal() -> None:
    # Given
    # an object
    obj1 = _EqualityFilterTestObject(1, 2)
    obj2 = _EqualityFilterTestObject(1, 3)

    # When
    # the objects are compared
    equal = obj1 == obj2

    # Then
    assert equal


def test_equal_with_chosen_attributes_not_equal() -> None:
    # Given
    # an object
    obj1 = _EqualityFilterTestObject(2, 1)
    obj2 = _EqualityFilterTestObject(3, 1)

    # When
    # the objects are compared
    equal = obj1 == obj2

    # Then
    assert not equal


def test_no_attributes_repr() -> None:
    # Given
    # an object
    obj = _ReprTestObject()
    # an expected repr
    expected_repr = (
        f"[test._ReprTestObject.{hex(id(obj))}]"
    )

    # When
    # the repr is obtained
    actual_repr = repr(obj)

    # Then
    # the repr is as expected
    assert actual_repr == expected_repr


def test_one_attribute_repr() -> None:
    # Given
    # an object
    obj = _ReprTestObject(number=1)
    # an expected repr
    expected_repr = '\n'.join((
        f"[test._ReprTestObject.{hex(id(obj))}]",
        f"  number: [int.{hex(id(1))}]",
        f"    1",
    ))

    # When
    # the repr is obtained
    actual_repr = repr(obj)

    # Then
    # the repr is as expected
    assert actual_repr == expected_repr


def test_several_attribute_repr() -> None:
    # Given
    # an object
    sub_obj = _ReprTestObject()
    obj = _ReprTestObject(number=1, string='foo', sub_obj=sub_obj)
    # an expected repr
    expected_repr = '\n'.join((
        f"[test._ReprTestObject.{hex(id(obj))}]",
        f"  number: [int.{hex(id(1))}]",
        f"    1",
        f"  string: [str.{hex(id('foo'))}]",
        f"    'foo'",
        f"  sub_obj: [test._ReprTestObject.{hex(id(sub_obj))}]",
    ))

    # When
    # the repr is obtained
    actual_repr = repr(obj)

    # Then
    # the repr is as expected
    assert actual_repr == expected_repr


def test_custom_indent() -> None:
    # Given
    # an object
    sub_obj = _ReprTestObject()
    obj = _ReprTestObject(number=1, string='foo', sub_obj=sub_obj)
    obj.set_indent('*')
    # an expected repr
    expected_repr = '\n'.join((
        f"[test._ReprTestObject.{hex(id(obj))}]",
        f"*number: [int.{hex(id(1))}]",
        f"**1",
        f"*string: [str.{hex(id('foo'))}]",
        f"**'foo'",
        f"*sub_obj: [test._ReprTestObject.{hex(id(sub_obj))}]",
    ))

    # When
    # the repr is obtained
    actual_repr = repr(obj)

    # Then
    # the repr is as expected
    assert actual_repr == expected_repr


def test_multiple_indent() -> None:
    # Given
    # an object
    sub_obj = _ReprTestObject(number=1)
    obj = _ReprTestObject(string='foo', sub_obj=sub_obj)
    # an expected repr
    expected_repr = '\n'.join((
        f"[test._ReprTestObject.{hex(id(obj))}]",
        f"  string: [str.{hex(id('foo'))}]",
        f"    'foo'",
        f"  sub_obj: [test._ReprTestObject.{hex(id(sub_obj))}]",
        f"    number: [int.{hex(id(1))}]",
        f"      1",
    ))

    # When
    # the repr is obtained
    actual_repr = repr(obj)

    # Then
    # the repr is as expected
    assert actual_repr == expected_repr


def test_custom_attributes() -> None:
    # Given
    # an object
    sub_obj = _ReprTestObject()
    obj = _ReprTestObject(number=1, string='foo', sub_obj=sub_obj)
    obj.customize_attributes('number', 'string')
    # an expected repr
    expected_repr = '\n'.join((
        f"[test._ReprTestObject.{hex(id(obj))}]",
        f"  number: [int.{hex(id(1))}]",
        f"    1",
        f"  string: [str.{hex(id('foo'))}]",
        f"    'foo'",
    ))

    # When
    # the repr is obtained
    actual_repr = repr(obj)

    # Then
    # the repr is as expected
    assert actual_repr == expected_repr


def test_custom_reprs() -> None:
    # Given
    # an object
    obj = _ReprTestObject(number=1, string='foo')
    # an expected repr
    expected_repr = '\n'.join((
        f"[test._ReprTestObject.{hex(id(obj))}]",
        f"  number: [int.{hex(id(1))}]",
        f"    1",
        f"  string: [str.{hex(id('foo'))}]",
        f"    'foo'",
    ))

    # When
    # the repr is obtained
    actual_repr = repr(obj)

    # Then
    # the repr is as expected
    assert actual_repr == expected_repr


def test_default_reprs() -> None:

    # Given
    # an object
    default_repr_obj = _FullNonRepr()
    obj = _ReprTestObject(default=default_repr_obj)
    # an expected repr
    expected_repr = '\n'.join((
        f"[test._ReprTestObject.{hex(id(obj))}]",
        f"  default: [test._FullNonRepr.{hex(id(default_repr_obj))}]",
        f"    value: [str.{hex(id(default_repr_obj.value))}]",
        f"      'a value'",
        f"    __module__: [str.{hex(id(default_repr_obj.__module__))}]",
        f"      'test'",
    ))

    # When
    # the repr is obtained
    actual_repr = repr(obj)

    # Then
    # the repr is as expected
    assert actual_repr == expected_repr, f"actual ({actual_repr}) != expected ({expected_repr})"


def test_list_iterable_repr() -> None:
    # Given
    # an object
    list_obj = [1, 2, 3]
    obj = _ReprTestObject(list_obj=list_obj)
    # an expected repr
    expected_repr = '\n'.join((
        f"[test._ReprTestObject.{hex(id(obj))}]",
        f"  list_obj: [list.{hex(id(list_obj))}]",
        f"    [int.{hex(id(1))}]",
        f"      1",
        f"    [int.{hex(id(2))}]",
        f"      2",
        f"    [int.{hex(id(3))}]",
        f"      3",
    ))

    # When
    # the repr is obtained
    actual_repr = repr(obj)

    # Then
    # the repr is as expected
    assert actual_repr == expected_repr


def test_default_named_repr_overridden() -> None:
    # Given
    # an obj with a callable as an attr
    def _external_func() -> None:
        pass

    class _Foo(din.ReprMixin):
        def __init__(self) -> None:
            super().__init__()
            self.external: typing.Optional[typing.Callable[..., None]] = None
    example_obj = _Foo()
    example_obj.external = _external_func
    # When
    # repr created
    f_repr = repr(example_obj)
    # Then
    # named repr ending not in repr
    assert f'at {hex(id(example_obj))}' not in f_repr
    # vulture
    example_obj.external  # pylint: disable=pointless-statement


def test_standard_dunder_repr() -> None:
    # Given
    # an obj with standard dunders
    class Normal:
        def __init__(self) -> None:
            self.__name__ = 'just_the_name'
            self.__qualname__ = 'this.is.the.name'
            self.__module__ = 'the_module'
            self.__file__ = 'the_file'

    thing = Normal()

    class Custom(din.ReprMixin):
        def __init__(self, thing: Normal) -> None:
            super().__init__()
            self.thing = thing

    custom = Custom(thing)

    # When
    # repr created
    f_repr = repr(custom)
    # Then
    # name does not up in repr
    # qualname does show up in repr
    # module does show up in repr
    # file does show up in repr
    assert '__name__' not in f_repr, f'__name__ found in {f_repr}'
    assert '__qualname__' in f_repr, f'__qualname__ not found in {f_repr}'
    assert '__module__' in f_repr, f'__module__ not found in {f_repr}'
    assert '__file__' in f_repr, f'__file__ not found in {f_repr}'
    assert 'just_the_name' not in f_repr, f'just_the_name found in {f_repr}'
    assert 'this.is.the.name' in f_repr, f'this.is.the.name not found in {f_repr}'
    assert 'the_module' in f_repr, f'the_module not found in {f_repr}'
    assert 'the_file' in f_repr, f'the_file not found in {f_repr}'

    # Vulture
    # pylint: disable=pointless-statement
    thing.__qualname__
    custom.thing
    # pylint: enable=pointless-statement


def test_just_name_repr() -> None:
    # Given
    # an obj with standard dunders
    class Normal:
        def __init__(self) -> None:
            self.__name__ = 'just_the_name'

    thing = Normal()

    class Custom(din.ReprMixin):
        def __init__(self, thing: Normal) -> None:
            super().__init__()
            self.thing = thing

    custom = Custom(thing)

    # When
    # repr created
    f_repr = repr(custom)
    # Then
    # name does not up in repr
    assert '__name__' in f_repr, f'__name__ not found in {f_repr}'
    assert 'just_the_name' in f_repr, f'just_the_name not found in {f_repr}'

    # Vulture
    # pylint: disable=pointless-statement
    custom.thing
    # pylint: enable=pointless-statement


def test_func_attr_repr() -> None:
    # Given
    # an obj with a callable as an attr
    def _external_func() -> None:
        pass

    class _Foo(din.ReprMixin):
        def __init__(self) -> None:
            super().__init__()
            self.external: typing.Optional[typing.Callable[..., None]] = None
    example_obj = _Foo()
    example_obj.external = _external_func
    # When
    # repr created
    f_repr = repr(example_obj)
    # Then
    # callable shows up in the repr
    assert 'external_func' in f_repr, f'external_func not in {f_repr}'
    # vulture
    example_obj.external  # pylint: disable=pointless-statement


def test_method_hidden() -> None:
    # Given
    # example obj
    class _Foo(din.ReprMixin):
        def my_method(self) -> None:
            """Do nothing."""
            pass

    example_obj = _Foo()
    # an expected repr
    expected_repr = '\n'.join((
        f"[test._Foo.{hex(id(example_obj))}]",
    ))
    # When
    # repr created
    f_repr = repr(example_obj)
    # Then
    # callable shows up in the repr
    assert f_repr == expected_repr
    # vulture
    example_obj.my_method  # pylint: disable=pointless-statement


def test_other_method_exposed() -> None:
    # Given
    # example obj
    class _Foo(din.ReprMixin):
        def my_method(self) -> None:
            """Do nothing."""
            pass

    other_obj = _Foo()

    class _Bar(din.ReprMixin):
        def __init__(self) -> None:
            super().__init__()
            self.other_method = other_obj.my_method

    example_obj = _Bar()
    # an expected repr
    expected_repr = '\n'.join((
        f"[test._Bar.{hex(id(example_obj))}]",
        f"  other_method: [method.{hex(id(example_obj.other_method))}]",
        f"    <bound method test_other_method_exposed.<locals>._Foo.my_method of [test._Foo.{hex(id(other_obj))}]>",
    ))
    # When
    # repr created
    f_repr = repr(example_obj)
    # Then
    # callable shows up in the repr
    assert f_repr == expected_repr


def test_static_method_hidden() -> None:
    # Given
    # example obj
    class _Foo(din.ReprMixin):
        @staticmethod
        def my_method() -> None:
            """Do nothing."""
            pass

    example_obj = _Foo()
    # an expected repr
    expected_repr = '\n'.join((
        f"[test._Foo.{hex(id(example_obj))}]",
    ))
    # When
    # repr created
    f_repr = repr(example_obj)
    # Then
    # callable shows up in the repr
    assert f_repr == expected_repr
    # vulture
    example_obj.my_method  # pylint: disable=pointless-statement


def test_other_static_method_exposed() -> None:
    # Given
    # example obj
    class _Foo(din.ReprMixin):
        @staticmethod
        def my_method() -> None:
            """Do nothing."""
            pass

    other_obj = _Foo()

    class _Bar(din.ReprMixin):
        def __init__(self) -> None:
            super().__init__()
            self.other_method = other_obj.my_method

    example_obj = _Bar()

    # When
    # repr created
    f_repr = repr(example_obj)

    # Then
    # callable shows up in the repr
    assert '_Foo.my_method' in f_repr, f"actual ({f_repr}) did not contain expected str ('_Foo.my_method')"


def test_class_method_hidden() -> None:
    # Given
    # example obj
    class _Foo(din.ReprMixin):
        @classmethod
        def my_method(cls) -> type:
            """Do nothing."""
            return cls

    example_obj = _Foo()
    # an expected repr
    expected_repr = '\n'.join((
        f"[test._Foo.{hex(id(example_obj))}]",
    ))
    # When
    # repr created
    f_repr = repr(example_obj)
    # Then
    # callable shows up in the repr
    assert f_repr == expected_repr
    # vulture
    example_obj.my_method  # pylint: disable=pointless-statement


def test_other_class_method_exposed() -> None:
    # Given
    # example obj
    class _Foo(din.ReprMixin):
        @classmethod
        def my_method(cls) -> type:
            """Do nothing."""
            return cls

    other_obj = _Foo()

    class _Bar(din.ReprMixin):
        def __init__(self) -> None:
            super().__init__()
            self.other_method = other_obj.my_method

    example_obj = _Bar()
    # an expected repr
    expected_repr = '\n'.join((
        f"[test._Bar.{hex(id(example_obj))}]",
        f"  other_method: [method.{hex(id(example_obj.other_method))}]",
        f"    <bound method test_other_class_method_exposed.<locals>._Foo.my_method of <class 'test.test_other_class_method_exposed.<locals>._Foo'>>",
    ))
    # When
    # repr created
    f_repr = repr(example_obj)
    # Then
    # callable shows up in the repr
    assert f_repr == expected_repr


def test_property_attr_exposed() -> None:
    # Given
    # example obj
    class _Foo(din.ReprMixin):
        @property
        def my_property(self) -> None:
            """Do nothing."""
            pass

    example_obj = _Foo()
    prop_value = example_obj.my_property
    # an expected repr
    expected_repr = '\n'.join((
        f"[test._Foo.{hex(id(example_obj))}]",
        f"  my_property: [NoneType.{hex(id(prop_value))}]",
        f"    None",
    ))
    # When
    # repr created
    f_repr = repr(example_obj)
    # Then
    # callable shows up in the repr
    assert f_repr == expected_repr


def test_dict_repr() -> None:
    # Given
    # an object
    key_1 = 'one'
    dict_obj = {key_1: 1, 2: 'two'}
    obj = _ReprTestObject(dict_obj=dict_obj)
    # an expected repr
    expected_repr = '\n'.join((
        f"[test._ReprTestObject.{hex(id(obj))}]",
        f"  dict_obj: [dict.{hex(id(dict_obj))}]",
        f"    [str.{hex(id(key_1))}]",
        f"      'one': [int.{hex(id(1))}]",
        f"      1",
        f"    [int.{hex(id(2))}]",
        f"      2: [str.{hex(id(dict_obj[2]))}]",
        f"      'two'",
    ))

    # When
    # the repr is obtained
    actual_repr = repr(obj)

    # Then
    # the repr is as expected
    assert actual_repr == expected_repr


def test_multiline_custom_repr() -> None:
    # Given
    # an object
    lines = ("first", "second", "third")
    string = '\n'.join(lines)
    obj = _ReprTestObject(string=string)
    # an expected repr
    expected_repr = '\n'.join((
        f"[test._ReprTestObject.{hex(id(obj))}]",
        f"  string: [str.{hex(id(string))}]",
        f"    'first'",
        f"    'second'",
        f"    'third'",
    ))

    # When
    # the repr is obtained
    actual_repr = repr(obj)

    # Then
    # the repr is as expected
    assert actual_repr == expected_repr


def test_recursive_repr() -> None:
    # Given
    # an object which recurses
    obj = _ReprRecursiveTestObject(one=1)
    # an expected repr
    expected_repr = '\n'.join((
        f"[test._ReprRecursiveTestObject.{hex(id(obj))}]",
        f"  one: [int.{hex(id(1))}]",
        f"    1",
        f"  recursive: [test._ReprRecursiveTestObject.{hex(id(obj))}]",
    ))

    # When
    # the repr is obtained
    actual_repr = repr(obj)

    # Then
    # the repr is as expected
    assert actual_repr == expected_repr


def test_repeated_repr() -> None:
    # Given
    # an object which repeats
    obj = _ReprTestObject(one=1, another_one=1)
    # an expected repr
    expected_repr = '\n'.join((
        f"[test._ReprTestObject.{hex(id(obj))}]",
        f"  another_one: [int.{hex(id(1))}]",
        f"    1",
        f"  one: [int.{hex(id(1))}]",
    ))

    # When
    # the repr is obtained
    actual_repr = repr(obj)

    # Then
    # the repr is as expected
    assert actual_repr == expected_repr


def test_independent_reprs() -> None:
    # Given
    # an object
    obj = _ReprTestObject(one=1)
    # another object with the same attributes
    other_obj = _ReprTestObject(one=1)
    # expected reprs for both
    expected_repr = '\n'.join((
        f"[test._ReprTestObject.{hex(id(obj))}]",
        f"  one: [int.{hex(id(1))}]",
        f"    1",
    ))
    other_expected_repr = '\n'.join((
        f"[test._ReprTestObject.{hex(id(other_obj))}]",
        f"  one: [int.{hex(id(1))}]",
        f"    1",
    ))

    # When
    # the repr is obtained
    actual_repr = repr(obj)
    other_actual_repr = repr(other_obj)

    # Then
    # the reprs are as expected
    assert actual_repr == expected_repr
    assert other_actual_repr == other_expected_repr


class _FrozenTestObject(din.FrozenMixin):
    def __init__(self) -> None:
        super().__init__()
        with self._thawed():
            self.my_string = 'original'


def test_frozen() -> None:
    # Given
    # a frozen object
    frozen = _FrozenTestObject()
    # an expected error
    expected_message = "Can't set my_string, because _FrozenTestObject is frozen."

    # When
    # an attribute is set
    try:
        frozen.my_string = 'changed!'
    except AttributeError as error:
        actual_message = str(error)

    # Then
    # an attribute error is raised
    assert expected_message == actual_message
    # the value didn't change.
    assert frozen.my_string == 'original'


def test_copy_frozen() -> None:
    # Given
    # a frozen object
    frozen = _FrozenTestObject()

    # When
    # object is copied with new data
    frozen_2 = frozen.copy_with(my_string='changed!')

    # Then
    # the old object's value didn't change.
    assert frozen.my_string == 'original'
    # the new object has the new value
    assert frozen_2.my_string == 'changed!'


def test_frozen_inheritance() -> None:
    # Given
    # a frozen object
    class _Inherited(_FrozenTestObject):
        def __init__(self) -> None:
            super().__init__()
            with self._thawed():
                self.additional_thing = "set in init"

    frozen = _Inherited()
    # an expected error
    expected_message = "Can't set additional_thing, because _Inherited is frozen."

    # When
    # an attribute is set
    try:
        frozen.additional_thing = 'changed!'
    except AttributeError as error:
        actual_message = str(error)

    # Then
    # an attribute error is raised
    assert expected_message == actual_message
    # the value didn't change.
    assert frozen.my_string == 'original'
    # vulture
    frozen.additional_thing  # pylint: disable=pointless-statement


def test_frozen_bad_thaw() -> None:
    # Given
    # a frozen object
    class _Inherited(_FrozenTestObject):
        def update(self) -> None:
            with self._thawed():
                self.my_string = "updated"

    frozen = _Inherited()
    # an expected error
    expected_message = "Can only thaw from __init__!"

    # When
    # an attribute is set
    try:
        frozen.update()
    except RuntimeError as error:
        actual_message = str(error)

    # Then
    # an attribute error is raised
    assert expected_message == actual_message
    # the value didn't change.
    assert frozen.my_string == 'original'


def test_generic_repr() -> None:
    # Given
    # an object
    TestType = typing.TypeVar('TestType')
    type_ = typing.Generic[TestType]  # pylint: disable=unsubscriptable-object
    obj = _ReprTestObject(type_=type_)
    # an expected repr
    expected_repr = '\n'.join((
        f"[test._ReprTestObject.{hex(id(obj))}]",
        f"  type_: [typing._GenericAlias.{hex(id(type_))}]",
    ))

    # When
    # the repr is obtained
    actual_repr = repr(obj)

    # Then
    # the repr is as expected
    assert actual_repr == expected_repr, f'actual ({actual_repr}) != expected ({expected_repr})'
