"""Version number."""


# [ API ]
VERSION = '0.5.0'


# [ Vulture ]
# API - only used presently in setup.py
VERSION  # pylint: disable=pointless-statement
